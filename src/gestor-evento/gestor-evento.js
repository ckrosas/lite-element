import { LitElement, html} from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement {
    static get properties(){
        return{

        };
    }
    
    constructor(){
        super();
    }

    render(){
        return html `
            <h3>Gestor Evento</h3>
            <emisor-evento @test.event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e){
        console.log("Se ha captura el evento del emisor");
        console.log(e.detail);
        this.shadowRoot.getElementById("receiver").course = e.detail.course;
        this.shadowRoot.getElementById("receiver").year = e.detail.year;
    }


}

customElements.define('gestor-evento', GestorEvento)