import { LitElement, html} from 'lit-element';

class TestBootstrap extends LitElement {

    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }

    render(){
        return html `
            <h3>Perobando Boostrap!</h3>
        `;
    }

}

customElements.define('ptest-bootstrap', TestBootstrap)