import { LitElement, html} from 'lit-element';

class PersonaStats extends LitElement {

    static get properties(){
        return{
            people: {type: Array}
        };
    }

    constructor(){
        super();
        this.people = [];
    }


    updated(changedProperties){
        console.log("updated en Personan-Stats");
        console.log(changedProperties);
        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en perosna-stats");
            let peopleStats = this.gatherPeopleArrayInfo(this.people);
            this.dispatchEvent(new CustomEvent("updated-people.stats",{
                detail:{
                    peopleStats: peopleStats
                }
            }))
        }
    }


    gatherPeopleArrayInfo(people){
        console.log("gatherPeopleArrayInfo en Personan-Stats");
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;

    }
}

customElements.define('persona-stats', PersonaStats)