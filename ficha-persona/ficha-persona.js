import { LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {
    
    static get properties(){
        return{
            name: {type: String},
            yearsInCompany: {type:Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
        
        }
    
        constructor(){
            super();
            this.name = "Prueba nombre";
            this.yearsInCompany = 3;
            this.photo = {
                src: "./img/persona.jpg",
                alt: "foto persona"
            };
            this.updatePersonInfo();

    }

    
    render(){
        return html `
            <div>Ficha Persona</div>
            <div>
                <label for="fname">Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br/>

                <label for="yearsInCompany">Años en la empresa</label>
                <input type="text" id="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br/>

                <input type="text" id="personInfo" value="${this.personInfo}" disabled></input>
                <br/>

                <img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"></img>

           </div>
        `;
    }

updated(changedProperties){
    if(changedProperties.has("name")){
        console.log("Propiedad name cambio su valor de " + changedProperties.get("name") + " a " + this.name);
    }

    if (changedProperties.has("yearsInCompany")) {
        console.log("Propiedad yearsInCompany cambio su valor de " + changedProperties.get("yearsInCompany") + " a " + this.yearsInCompany);
        this.updatePersonInfo();
    }
    if (changedProperties.has("personInfo")) {
        console.log("Propiedad personInfo cambio su valor de " + changedProperties.get("personInfo") + " a " + this.personInfo);
    }
}

updateName(e){
    console.log("updateName");
    this.name = e.target.value;
}

updatePersonInfo(){
    console.log("updatePesonInfo");
    console.log("yearsInCompany vale " + this.yearsInCompany);
    if(this.yearsInCompany >= 7){
        this.personInfo = "lead";
    } else if (this.yearsInCompany >=5){
        this.personInfo = "senior"
    } else if (this.yearsInCompany >=3){
        this.personInfo = "team"
    } else {
        this.personInfo = "junior"
    }
}

updateYearsInCompany(e){
    console.log("updateYearsInCompany");
    this.yearsInCompany = e.target.value;
}
}

customElements.define('ficha-persona', FichaPersona)

//MIO